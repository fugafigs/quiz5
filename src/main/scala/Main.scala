object Main extends App {
  val numbers = (1 to 9).toList
  val operators1 = List("|", "")
  val operators2 = List("-", "+")
  val evalValue  = Map[String, (Int, Int) => Int](("-", _ - _), ("+" ,_ + _), ("", _ + _))

  def build(List : List[Int], operators : List[String], text : String, result : List[String]) : List[String] = {
    if (!List.isEmpty) {
      operators.foldLeft(result) { (r, op) => 
        build(List.tail, operators1, text + op + List.head, r) 
      }
    } else {
      text :: result
    }
  }

  def build2(List : List[Int], operators : List[String], text : String, value : Int, result : List[(String, Int)]) : List[(String, Int)] = {
    if (!List.isEmpty) {
      operators.foldLeft(result) { (r, op) =>
        build2(List.tail, operators2, text + op + List.head, evalValue(op)(value, List.head), r)
      }
    } else {
      (text, value) :: result
    }
  }

  for (sequences <- build(numbers, operators1.tail, "", List.empty)) {
    val terms = sequences.split('|').toList.map(_.toInt)
    for (result <- build2(terms, operators1.tail, "", 0, List.empty); 
         if result._2 == 100
    ) {
      println(result)
    }
  }
}

